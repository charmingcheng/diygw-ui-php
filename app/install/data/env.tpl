APP_DEBUG = true

[APP]
DEFAULT_TIMEZONE = Asia/Shanghai

[DATABASE]
TYPE = mysql
HOSTNAME = [hostname]
DATABASE = [database]
USERNAME = [username]
PASSWORD = [password]
HOSTPORT = [hostport]
CHARSET = utf8mb4
DEBUG = true

[LANG]
default_lang = zh-cn

[JWT]
SECRET = [secret]
